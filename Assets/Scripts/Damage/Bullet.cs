﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 5;
    public float lifeTime = 3;

    [HideInInspector]
    public BulletObject bulletPool;

    protected Rigidbody2D rigid;

    protected virtual void OnEnable()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    public virtual void Fly(Vector2 direction)
    {
        rigid.velocity = direction * speed;
        Invoke("Kill", lifeTime);
    }

    public void Kill()
    {
        bulletPool.ReturnToPool();
        //Destroy(gameObject, delay);
    }
}