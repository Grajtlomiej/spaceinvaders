﻿using UnityEngine;

public class CollisionContactDamager : Damager
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        if ((hittableLayers.value & 1 << collision.gameObject.layer) == 0)
            return;
        
        Damage(collision.collider);
    }
}