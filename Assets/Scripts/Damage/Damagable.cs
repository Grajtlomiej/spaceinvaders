﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Damagable : MonoBehaviour
{
    [Serializable]
    public class HealthEvent : UnityEvent<Damagable> { }

    [Serializable]
    public class DamageEvent : UnityEvent<DamageMessage, Damagable> { }

    [Serializable]
    public class HealEvent : UnityEvent<int, Damagable> { }


    [Header("Settings")]
    public int startingHealth = 5;
    public bool invulnerableAfterDamage = true;
    public float invulnerabilityDuration = 3f;
    public bool disableOnDeath = false;
    public Vector2 centreOffset = new Vector2(0f, 1f);

    [Header("Events")]
    public HealthEvent OnHealthSet;
    public DamageEvent OnTakeDamage;
    public DamageEvent OnDie;
    public HealEvent OnGainHealth;
    public DamageEvent OnCantTakeDamage;

    [HideInInspector]
    public DamageMessage message;

    protected bool invulnerable;
    protected int currentHealth;

    protected Coroutine inulnerabilityCoroutine;
    
    public float CurrentHealthPercentage
    {
        get { return (float)currentHealth/startingHealth; }
    }

    public int CurrentHealth
    {
        get { return currentHealth; }
    }

    void Start()
    {
        currentHealth = startingHealth;
        OnHealthSet.Invoke(this);
        DisableInvulnerability();
    }

    public void EnableInvulnerability(bool ignoreTimer = false)
    {
        invulnerable = true;
        if(!ignoreTimer)
            inulnerabilityCoroutine = StartCoroutine(InulnerabilityCoroutine());
    }

    public void DisableInvulnerability()
    {
        if(inulnerabilityCoroutine != null)
            StopCoroutine(inulnerabilityCoroutine);
        invulnerable = false;
    }

    private IEnumerator InulnerabilityCoroutine()
    {
        invulnerable = true;
        yield return new WaitForSeconds(invulnerabilityDuration);
        invulnerable = false;
    }

    public bool TakeDamage(DamageMessage damageMessage)
    {
        if ((invulnerable && !message.ignoreInvincible) || currentHealth <= 0)
            return true;

        message = damageMessage;

        if (!invulnerable)
        {
            currentHealth -= message.amount;
            OnHealthSet.Invoke(this);
        }
        
        if (currentHealth <= 0)
        {
            OnDie.Invoke(message, this);
            if (disableOnDeath)
                gameObject.SetActive(false);

            StopAllCoroutines();
        }
        else
            OnTakeDamage.Invoke(message, this);
        
        if(invulnerableAfterDamage)
            EnableInvulnerability();
        return true;
    }

    public void GainHealth(int amount)
    {
        currentHealth += amount;

        if (currentHealth > startingHealth)
            currentHealth = startingHealth;

        OnHealthSet.Invoke(this);
        OnGainHealth.Invoke(amount, this);
    }

    public void SetHealth(int amount)
    {
        currentHealth = amount;
        startingHealth = amount;
        OnHealthSet.Invoke(this);
    }

    public struct DamageMessage
    {
        public MonoBehaviour damager;
        public int amount;
        public bool ignoreInvincible;
        public Vector2 direction;
        public Vector3 damageSource;
    } 
}