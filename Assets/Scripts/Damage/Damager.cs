﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Damager : MonoBehaviour
{
    [Serializable]
    public class DamagableEvent : UnityEvent<Damagable.DamageMessage, Damagable> { }

    public int damage = 1;
    public bool ignoreInvincibility = false;
    public LayerMask hittableLayers;
    public Vector2 size;
    public DamagableEvent OnDamageableHit;
    public UnityEvent OnNonDamageableHit;
    public Transform damageSource;

    private void Start()
    {
        if(damageSource == null)
            damageSource = transform;
    }

    public void Damage()
    {
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, size, 0, hittableLayers);
        for (int i = 0; i < hits.Length; i++)
        {
            Damage(hits[i]);
        }
    }

    public void Damage(Collider2D collider)
    {
        Damagable damageable = collider.GetComponent<Damagable>();
        if (damageable && damageable.enabled)
        {
            Damagable.DamageMessage message = new Damagable.DamageMessage
            {
                damageSource = damageSource.position,
                damager = this,
                amount = damage,
                direction = ((damageable.transform.position + (Vector3)damageable.centreOffset) - damageSource.position).normalized,
                ignoreInvincible = ignoreInvincibility,
            };

            if(damageable.TakeDamage(message))
            {
                if (OnDamageableHit != null)
                    OnDamageableHit.Invoke(message, damageable);
            }
        }
        else
        {
            if (OnNonDamageableHit != null)
                OnNonDamageableHit.Invoke();
        }
    }
    
#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, size);
    }
#endif
}