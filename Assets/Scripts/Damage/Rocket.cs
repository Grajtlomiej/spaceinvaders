﻿using UnityEngine;

public class Rocket : Bullet
{
    private float turn = 2.5f;
    private float lastTurn = 0f;
    private Transform trans;
    private Vector3 target;

    protected override void OnEnable()
    {
        rigid = GetComponent<Rigidbody2D>();
        trans = transform;
        turn = 2.5f;
        lastTurn = 0f;
    }

    public override void Fly(Vector2 direction)
    {
        target = trans.position + (Vector3)direction * 10f;
        Invoke("Kill", lifeTime);
    }

    void FixedUpdate()
    {
        Quaternion newRotation = Quaternion.LookRotation(trans.position - target, Vector3.forward);
        newRotation.x = 0.0f;
        newRotation.y = 0.0f;
        trans.rotation = Quaternion.Slerp(trans.rotation, newRotation, Time.fixedDeltaTime * turn);
        rigid.velocity = trans.up * speed;
        if (turn < 40f)
        {
            lastTurn += Time.deltaTime * Time.deltaTime * 50f;
            turn += lastTurn;
        }
    }

}