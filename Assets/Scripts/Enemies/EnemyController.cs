﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public int score = 1;

    private readonly string explosion = "Explosion";

    void Start()
    {
        EnemiesManager.Instance.RegisterEnemy(this);
    }

    public void OnHit()
    {
        VFXManager.Instance.Drop(explosion, transform.position);
    }

    public void OnDie()
    {
        VFXManager.Instance.Drop(explosion, transform.position);
        ScoreManager.Instance.AddScore(score);
        EnemiesManager.Instance.RemoveEnemy(this);
    }
}