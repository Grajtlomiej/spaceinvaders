﻿using System.Collections;
using UnityEngine;

public class IntervalShooting : MonoBehaviour
{
    public float interval = 1f;
    public BulletPool pool;
    public Transform[] origins;

    private WaitForSeconds wait;

    private readonly string gunfire = "Gunfire";

    void Start()
    {
        wait = new WaitForSeconds(interval);
        StartCoroutine(Shooting());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    IEnumerator Shooting()
    {
        while (true)
        {
            Shoot();
            yield return wait;
        }
    }

    private void Shoot()
    {
        for (int i = 0; i < origins.Length; i++)
        {
            BulletObject bullet = pool.Pop(origins[i].position);
            bullet.bullet.Fly(Vector2.down);
            VFXManager.Instance.Drop(gunfire, origins[i].position);
        }
    }

}