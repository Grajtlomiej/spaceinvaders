﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemiesManager : MonoBehaviour
{
    [HideInInspector]
    public List<EnemyController> enemies = new List<EnemyController>();

    public UnityAction onEnemyRemoved;
 
    private static EnemiesManager instance;

    public static EnemiesManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<EnemiesManager>();

            return instance;
        }
    }

    public void RegisterEnemy(EnemyController enemy)
    {
        if (!enemies.Contains(enemy))
            enemies.Add(enemy);
    }

    public void RemoveEnemy(EnemyController enemy)
    {
        if (enemies.Contains(enemy))
            enemies.Remove(enemy);

        if (onEnemyRemoved != null)
            onEnemyRemoved.Invoke();
    }

}