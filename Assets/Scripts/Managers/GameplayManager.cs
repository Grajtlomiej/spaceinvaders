﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    public float startDelay = 1f;
    public float restartDelay = 1f;

    private void Start()
    {
        PlayerController.Instance.DisableMovement();
        PlayerController.Instance.DisableShooting();
        Invoke("StartScene", startDelay);
        EnemiesManager.Instance.onEnemyRemoved += OnEnemyRemoved;
        PlayerController.Instance.GetComponent<Damagable>().OnDie.AddListener(OnPlayerDie);
    }

    private void StartScene()
    {
        PlayerController.Instance.EnableMovement();
        PlayerController.Instance.EnableShooting();
    }

    void OnEnemyRemoved()
    {
        if (EnemiesManager.Instance.enemies.Count == 0)
            Invoke("RestartScene", restartDelay);
    }

    void OnPlayerDie(Damagable.DamageMessage message, Damagable damagable)
    {
        Invoke("RestartScene", restartDelay);
    }

    public void RestartScene()
    {
        EnemiesManager.Instance.onEnemyRemoved -= OnEnemyRemoved;
        PlayerController.Instance.GetComponent<Damagable>().OnDie.RemoveListener(OnPlayerDie);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}