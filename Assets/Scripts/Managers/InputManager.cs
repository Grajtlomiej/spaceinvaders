﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager instance;

    private readonly string horizontal = "Horizontal";
    private readonly KeyCode space = KeyCode.Space;
    private readonly KeyCode tab = KeyCode.Tab;

    public static InputManager Instance 
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<InputManager>();

            if(instance == null)
            {
                GameObject inputObject = new GameObject("InputManager");
                instance = inputObject.AddComponent<InputManager>();
            }

            return instance;
        }
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    public float GetHorizontalValue()
    {
        return Input.GetAxis(horizontal);
    }

    public bool FireHold()
    {
        return Input.GetKey(space);
    }

    public bool NextWeapon()
    {
        return Input.GetKeyDown(tab);
    }

}