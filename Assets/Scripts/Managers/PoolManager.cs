﻿using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public BulletPool[] pools;

    private static PoolManager instance;

    public static PoolManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<PoolManager>();

            return instance;
        }
    }

    public BulletPool GetPool(GameObject prefab)
    {
        for (int i = 0; i < pools.Length; i++)
        {
            if (pools[i].prefab == prefab)
                return pools[i];
        }

        return null;
    }
}