﻿using UnityEngine;
using UnityEngine.Events;

public class ScoreManager : MonoBehaviour
{
    public UnityAction<int> onScoreChanged;

    private int score;

    private static ScoreManager instance;

    public static ScoreManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<ScoreManager>();

            return instance;
        }
    }

    public void AddScore(int value)
    {
        score += value;
        if (onScoreChanged != null)
            onScoreChanged.Invoke(score);
    }

}