﻿using UnityEngine;
using System;

public class VFXManager : MonoBehaviour
{
    public VfxData[] vfxs;

    private static VFXManager instance;

    public static VFXManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<VFXManager>();

            return instance;
        }
    }

    public void Drop(string name, Vector3 position)
    {
        for (int i = 0; i < vfxs.Length; i++)
        {
            if(vfxs[i].vfxName.Equals(name))
            {
                vfxs[i].pool.Pop(position);
            }
        }
    }
}

[Serializable]
public class VfxData
{
    public string vfxName;
    public VfxPool pool;
}