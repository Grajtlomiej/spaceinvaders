﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float flickeringDuration = .1f;

    public Transform[] standardCannons;
    public Transform[] rocketsCannons;

    private PlayerMovement movement;
    private PlayerShooting shooting;
    private Damagable damagable;
    private SpriteRenderer spriteRenderer;

    private readonly string explosion = "Explosion";

    private static PlayerController instance;

    public static PlayerController Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<PlayerController>();

            if (instance == null)
                Debug.LogError("Please add the player to the scene!");

            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        movement = GetComponent<PlayerMovement>();
        shooting = GetComponent<PlayerShooting>();
        damagable = GetComponent<Damagable>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void EnableShooting()
    {
        shooting.enabled = true;
    }

    public void DisableShooting()
    {
        shooting.enabled = false;
    }

    public void EnableMovement()
    {
        movement.enabled = true;
    }

    public void DisableMovement()
    {
        movement.enabled = false;
    }

    public void OnHit(Damagable.DamageMessage damageMessage, Damagable damagable)
    {
        VFXManager.Instance.Drop(explosion, transform.position);
        StartCoroutine(Flicker());
    }

    public void OnDie()
    {
        DisableShooting();
        DisableMovement();
        VFXManager.Instance.Drop(explosion, transform.position);
    }

    private IEnumerator Flicker()
    {
        float timer = 0f;
        while (timer < damagable.invulnerabilityDuration)
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
            yield return new WaitForSeconds(flickeringDuration);
            timer += flickeringDuration;
        }

        spriteRenderer.enabled = true;
    }
}