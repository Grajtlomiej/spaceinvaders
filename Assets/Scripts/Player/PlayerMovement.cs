﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 3f;
    public float minXPosition;
    public float maxXPosition;

    private Rigidbody2D rigid;
    private InputManager inputManager;

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        inputManager = InputManager.Instance;
    }

    private void OnDisable()
    {
        rigid.velocity = Vector2.zero;
    }

    void FixedUpdate()
    {
        rigid.velocity = new Vector2(inputManager.GetHorizontalValue() * speed, 0);
        rigid.position = new Vector2(Mathf.Clamp(rigid.position.x, minXPosition, maxXPosition), rigid.position.y);
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(new Vector3(minXPosition, transform.position.y, 0), new Vector3(maxXPosition, transform.position.y, 0));
    }
#endif

}