﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public WeaponBase[] weapons;

    private int currentWeapon;
    private float lastShoot = -1000;
    private InputManager inputManager;

    void Start()
    {
        currentWeapon = 0;
        inputManager = InputManager.Instance;
    }

    void Update()
    {
        if (CanShoot())
            Shoot();

        CheckChangeWeapon();
    }

    private bool CanShoot()
    {
        return lastShoot + weapons[currentWeapon].cooldown < Time.time && inputManager.FireHold();
    }

    private void Shoot()
    {
        lastShoot = Time.time;
        weapons[currentWeapon].Shoot();
    }

    private void CheckChangeWeapon()
    {
        if(inputManager.NextWeapon())
        {
            currentWeapon = (int)Mathf.Repeat(currentWeapon + 1, weapons.Length);
            lastShoot = -1000;
        }
    }

}