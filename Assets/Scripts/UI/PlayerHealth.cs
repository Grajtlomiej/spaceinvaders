﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public Image[] icons;

    private Damagable playerDamagable;

    private void Awake()
    {
        playerDamagable = PlayerController.Instance.GetComponent<Damagable>();
    }

    private void OnEnable()
    {
        playerDamagable.OnHealthSet.AddListener(OnHealthSet);
    }

    private void OnDisable()
    {
        playerDamagable.OnHealthSet.RemoveListener(OnHealthSet);
    }

    void OnHealthSet(Damagable damagable)
    {
        if(damagable.startingHealth != icons.Length)
        {
            Debug.LogError("Please update the number of icons");
            return;
        }

        for (int i = 0; i < icons.Length; i++)
        {
            icons[i].enabled = i < damagable.CurrentHealth;
        }
    }
}