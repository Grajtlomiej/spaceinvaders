﻿using UnityEngine;
using TMPro;

public class PlayerScore : MonoBehaviour
{
    public TextMeshProUGUI text;

    private void OnEnable()
    {
        ScoreManager.Instance.onScoreChanged += OnScoreChanged;
    }

    private void OnDisable()
    {
        if(ScoreManager.Instance)
            ScoreManager.Instance.onScoreChanged -= OnScoreChanged;
    }

    void OnScoreChanged(int value)
    {
        text.text = value.ToString();
    }

}