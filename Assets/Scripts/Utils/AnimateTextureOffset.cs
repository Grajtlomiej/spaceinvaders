﻿using UnityEngine;

public class AnimateTextureOffset : MonoBehaviour
{
    public float speed = 1.5f;

    private Renderer rend;

    void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        rend.material.mainTextureOffset = new Vector2(0, Time.time * speed);
    }
}