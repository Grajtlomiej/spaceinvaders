﻿using System.Collections.Generic;
using UnityEngine;

public class BulletPool : ObjectPool<BulletPool, BulletObject, Vector2>
{
    static protected Dictionary<GameObject, BulletPool> s_PoolInstances = new Dictionary<GameObject, BulletPool>();

    private void Awake()
    {
        if (prefab != null && !s_PoolInstances.ContainsKey(prefab))
            s_PoolInstances.Add(prefab, this);
    }

    private void OnDestroy()
    {
        s_PoolInstances.Remove(prefab);
    }

    static public BulletPool GetObjectPool(GameObject prefab, int initialPoolCount = 10)
    {
        BulletPool objPool = null;
        if (!s_PoolInstances.TryGetValue(prefab, out objPool))
        {
            GameObject obj = new GameObject(prefab.name + "_Pool");
            objPool = obj.AddComponent<BulletPool>();
            objPool.prefab = prefab;
            objPool.initialPoolCount = initialPoolCount;

            s_PoolInstances[prefab] = objPool;
        }

        return objPool;
    }
}

public class BulletObject : PoolObject<BulletPool, BulletObject, Vector2>
{
    public Transform transform;
    public Bullet bullet;

    protected override void SetReferences()
    {
        transform = instance.transform;
        bullet = instance.GetComponent<Bullet>();
        bullet.bulletPool = this;
    }

    public override void WakeUp(Vector2 position)
    {
        transform.position = position;
        instance.SetActive(true);
    }

    public override void Sleep()
    {
        instance.SetActive(false);
    }
}