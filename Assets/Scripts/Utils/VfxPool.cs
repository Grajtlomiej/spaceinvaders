﻿using System.Collections.Generic;
using UnityEngine;

public class VfxPool : ObjectPool<VfxPool, VfxObject, Vector2>
{
    static protected Dictionary<GameObject, VfxPool> s_PoolInstances = new Dictionary<GameObject, VfxPool>();

    private void Awake()
    {
        if (prefab != null && !s_PoolInstances.ContainsKey(prefab))
            s_PoolInstances.Add(prefab, this);
    }

    private void OnDestroy()
    {
        s_PoolInstances.Remove(prefab);
    }

    static public VfxPool GetObjectPool(GameObject prefab, int initialPoolCount = 10)
    {
        VfxPool objPool = null;
        if (!s_PoolInstances.TryGetValue(prefab, out objPool))
        {
            GameObject obj = new GameObject(prefab.name + "_Pool");
            objPool = obj.AddComponent<VfxPool>();
            objPool.prefab = prefab;
            objPool.initialPoolCount = initialPoolCount;

            s_PoolInstances[prefab] = objPool;
        }

        return objPool;
    }
}

public class VfxObject : PoolObject<VfxPool, VfxObject, Vector2>
{
    public Transform transform;
    public Vfx vfx;

    protected override void SetReferences()
    {
        transform = instance.transform;
        vfx = instance.GetComponent<Vfx>();
        vfx.pool = this;
    }

    public override void WakeUp(Vector2 position)
    {
        transform.position = position;
        instance.SetActive(true);
    }

    public override void Sleep()
    {
        instance.SetActive(false);
    }
}