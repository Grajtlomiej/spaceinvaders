﻿using UnityEngine;

public class Vfx : MonoBehaviour
{
    public float duration = 1f;

    [HideInInspector]
    public VfxObject pool;


    private void OnEnable()
    {
        Invoke("ReturnToPool", duration);
    }

    private void ReturnToPool()
    {
        pool.ReturnToPool();
    }

}