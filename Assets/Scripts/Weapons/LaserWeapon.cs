﻿using UnityEngine;

[CreateAssetMenu(fileName = "Laser", menuName = "Weapons/Laser")]
public class LaserWeapon : WeaponBase
{
    public override void Shoot()
    {
        base.Shoot();
        Transform[] origins = PlayerController.Instance.standardCannons;
        for (int i = 0; i < origins.Length; i++)
        {
            BulletObject bulletObject = PoolManager.Instance.GetPool(bullet).Pop(origins[i].position);
            bulletObject.bullet.Fly(Vector2.up);
            VFXManager.Instance.Drop(gunfire, origins[i].position);
        }
    }
}