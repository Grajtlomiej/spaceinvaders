﻿using UnityEngine;

[CreateAssetMenu(fileName = "Rocket", menuName = "Weapons/Rocket")]
public class RocketWeapon : WeaponBase
{
    public override void Shoot()
    {
        base.Shoot();
        Transform[] origins = PlayerController.Instance.rocketsCannons;
        for (int i = 0; i < origins.Length; i++)
        {
            BulletObject bulletObject = PoolManager.Instance.GetPool(bullet).Pop(origins[i].position);
            bulletObject.transform.rotation = origins[i].rotation;
            bulletObject.bullet.Fly(Vector2.up);
            VFXManager.Instance.Drop(gunfire, origins[i].position);
        }
    }
}