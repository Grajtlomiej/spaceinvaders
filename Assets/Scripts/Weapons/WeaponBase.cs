﻿using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Weapons")]
public class WeaponBase : ScriptableObject
{
    public string weaponName;
    public Sprite icon;
    public float cooldown = .2f;
    public GameObject bullet;

    protected readonly string gunfire = "Gunfire";

    public virtual void Shoot()
    { }
}